<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page session="true"%>
<c:set var="now" value="<%=new java.util.Date()%>"/>
<c:set var="context" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap-select.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/ajax-bootstrap-select.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/datepicker.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/ion.rangeSlider.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/ion.rangeSlider.skinHTML5.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/my.css?${uuid}" />" />

    <title>FINEASY</title>
    <script>
        validationUrl = "<c:url value="/form/validation/" />";
        contextPath = "<c:url value="/" />";
        defaultLang = "${lang}";
        sessionId = "${pageContext.session.id}";
    </script>
    <style>
        html,body {
            height: 100%;
        }
        #yellow {
            height: 100%;
            background: yellow;
        }
    </style>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WTRJL9Z');</script>
    <!-- End Google Tag Manager -->
</head>
<body class="" style="background-color: #4e555b">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WTRJL9Z"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<table style="height: 100%; width: 100%;">
    <tbody>
        <tr>
            <td class="align-middle text-center" style="padding: 20px;">
                <div class="container bg-white" style="padding: 20px; border-radius: 5px;">
                    <div>
                        <span style="font-size: 96px; color: Dodgerblue;">
                            <i class="far fa-smile"></i>
                        </span>
                    </div>
                    <h3>Terima kasih</h3>
                    <h6>${message}</h6>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width: 48px">
            <span class="fa fa-spinner fa-spin fa-3x"></span>
        </div>
    </div>
</div>

<script src="<c:url value="/js/lib/jquery-3.3.1.js" />"></script>
<script src="<c:url value="/custom/script/startup.min.js" />" type="text/javascript"></script>


</body>
</html>