<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
$(function() {

    contextPath = "<c:url value="/" />";

    console.log("load scrypts under "+contextPath+"...");
    jQuery.cachedScript = function( url, options ) {
        // Allow user to set any option except for dataType, cache, and url
        options = $.extend( options || {}, {
            cache: true,
            dataType: "script",
            url: url
        });

        // Use $.ajax() since it is more flexible than $.getScript
        // Return the jqXHR object so we can chain callbacks
        return jQuery.ajax( options );
    };

    function S4() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    }

    $.cachedScript(contextPath+"js/lib/jquery.number.min.js").done(function( script, textStatus ) {});

    $.cachedScript(contextPath+"js/lib/popper.min.js").done(function( script, textStatus ) {
        $.cachedScript(contextPath+"js/lib/bootstrap.min.js").done(function( script, textStatus ) {
            $.cachedScript(contextPath+"js/lib/bootstrap-select.min.js").done(function( script, textStatus ) {
                $.cachedScript(contextPath+"js/lib/ajax-bootstrap-select.min.js").done(function( script, textStatus ) {
                    $.cachedScript(contextPath+"js/select-picker.js").done(function( script, textStatus ) {});
                    $.cachedScript(contextPath+"custom/script/${uuid}/my.js").done(function( script, textStatus ) {});
                });
            });

            $.cachedScript(contextPath+"js/lib/moment.min.js").done(function( script, textStatus ) {
                $.cachedScript(contextPath+"js/lib/bootstrap-datepicker.js").done(function( script, textStatus ) {
                    $.cachedScript(contextPath+"js/date-picker.js").done(function( script, textStatus ) {});
                });
            });

            $.cachedScript(contextPath+"js/lib/ion.rangeSlider.min.js").done(function( script, textStatus ) {
                $.cachedScript(contextPath+"custom/script/${uuid}/loan-slider.js").done(function( script, textStatus ) {});
            });

        });
    });

    $.cachedScript(contextPath+"js/lib/parsley.min.js").done(function( script, textStatus ) {
        $.cachedScript(contextPath+"js/lib/parsley-locales/${lang}.js").done(function( script, textStatus ) {});
        $.cachedScript(contextPath+"custom/script/${uuid}/validation.js").done(function( script, textStatus ) {});
    });
    $.cachedScript(contextPath+"js/lib/jquery.mask.min.js").done(function( script, textStatus ) {
        $.cachedScript(contextPath+"custom/script/${uuid}/mask.js?aaa").done(function( script, textStatus ) {});
    });
});

