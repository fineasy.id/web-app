<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>

    var loanData = {};

    var calculateLoan = function() {
        var amount = $("#amount_id").val();
        var tenor = $("#tenor_id").val();
        var charge = loanData["charge"][amount][tenor];
        //console.log("Amount: "+amount+", tenor: "+tenor+", charge: "+charge);
        $("#amount_loan").text($.number( amount, 0, ',', '.' ));
        $("#amount_charge").text($.number( charge, 0, ',', '.' ));
    }

    $("#amount_id").ionRangeSlider({
        grid: true,
        prefix: "Rp ",
        prettify_enabled: true,
        prettify_separator: ".",
        values: [],
        onChange: function (data) {
            calculateLoan();
        },
        onUpdate: function (data) {
            calculateLoan();
        }
    });

    var sliderAmount = $("#amount_id").data("ionRangeSlider");

    $("#tenor_id").ionRangeSlider({
        grid: true,
        postfix: ' ${days}',
        prettify_enabled: true,
        prettify_separator: ".",
        values: [],
        onChange: function (data) {
            calculateLoan();
        },
        onUpdate: function (data) {
            calculateLoan();
        }
    });

    var sliderTenor = $("#tenor_id").data("ionRangeSlider");

    var reloadSlider = function() {
        var container = $("#container-slider");
        container.css("display","none");

        $.ajax({
            dataType: "json",
            url: "<c:url value="/custom/data/loan-matrix.json"/>",
            method: 'GET',
            data: {
                test: "yes"
            },
            success: function(data) {
                loanData = data;
                sliderAmount.update({
                    values: data["amount"],
                    from: 0
                });
                sliderTenor.update({
                    values: data["tenor"],
                    from: 0
                });
                container.css("display","block");
            }
        });
    }

    reloadSlider();



    $("#reload-button").on("click", function() {
        reloadSlider();
    })





