<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
$(function() {

    var formData = ${formData};

    var f1 = $("#form1").parsley({
        errorClass: 'is-invalid text-danger',
        successClass: 'is-valid', // Comment this option if you don't want the field to become green when valid. Recommended in Google material design to prevent too many hints for user experience. Only report when a field is wrong.
        errorsWrapper: '<div class="form-text validation-error"></div>',
        errorTemplate: '<div></div>',
        trigger: 'change',
        requiredMessage: "${msg_field_required}"
    });

    var updateDb = true;

    Parsley.addValidator('wordcount', {
        validateString: function(fieldValue, minWord, instance) {
            var s = fieldValue;
            s = s.replace(/(^\s*)|(\s*$)/gi,"");
            s = s.replace(/[ ]{2,}/gi," ");
            s = s.replace(/\n /,"\n");
            var wc = s.split(' ').length;
            if (wc < parseInt(minWord)) return false;
            else {
                return true;
            }
        },
        messages: {
            en: "${msg_name_invalid}",
            id: "${msg_name_invalid}"
        }
    });

    Parsley.addValidator('localdate', {
        validateString: function(fieldValue, fieldName, instance) {
            var s = fieldValue.split("/");
            if (s.length < 3) return false;
            if (parseInt(s[0]) > 31) return false;
            if (parseInt(s[1]) > 12) return false;
            if (parseInt(s[2]) > (new Date()).getFullYear()) return false;
        },
        messages: {
            en: "${msg_date_invalid}",
            id: "${msg_date_invalid}"
        }
    });


    Parsley.on('field:success', function() {
        // This global callback will be called for any field that fails validation.
        var updateDb = false;
        var fieldName = this.$element.attr("name");
        var fieldValue = this.$element.val();
        var fieldId = this.$element.attr("id");

        if (!formData[fieldId]) {
            //console.log("Previous value ["+fieldId+"] doesn't exists");
            formData[fieldId] = fieldValue;
            updateDb = true;
        } else if (formData[fieldId] != fieldValue) {
            //console.log("Previous value ["+fieldId+"] different");
            formData[fieldId] = fieldValue;
            updateDb = true;
        } else {
            //console.log("Nothing ["+fieldId+"] is changed");
            updateDb = false;
        }

        if (updateDb) {
            updateToServer(fieldId, fieldName, fieldValue);
        }
    });

    var updateToServer = function(fieldId, fieldName, fieldValue) {
        console.log("Update to database for ["+fieldName+"]: "+fieldValue);
        $.ajax({
            url: "<c:url value="/form/update/" />"+fieldId,
            method:'POST',
            data: {
                i: fieldId,
                v: fieldValue,
                n: fieldName,
                "${_csrf.parameterName}": "${_csrf.token}"
            }
        }).done(function (json) {

        })
    }

    $("#submit").click(function () {
        updateDb = false;
        var success = f1.validate();
        console.log("is success? "+success);
        if (f1.validate()) {
            $.post('<c:url value="/" />', $('#form1').serialize())
                .done(function(json){
                    setTimeout(function () {
                        console.log("hide modal...")
                        $('#modal-block').modal('hide');

                        if (json.success) {
                            window.location.href = "<c:url value="/thank" />";
                        } else {
                            window.location.href = "<c:url value="/sorry" />";
                        }

                    }, 1000);
                })
                .fail(function() {
                    setTimeout(function () {
                        console.log("hide modal...")
                        $('#modal-block').modal('hide');
                        window.location.href = "<c:url value="/failed" />";
                    }, 1000);
                })
        }
    });

});