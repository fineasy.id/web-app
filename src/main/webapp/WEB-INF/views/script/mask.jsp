<%@ page language="java" contentType="application/javascript; charset=UTF-8" pageEncoding="UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@page session="true"%><c:set var="context" value="${pageContext.request.contextPath}" />//<script>
$(function() {
    $('#mobile').mask('00000000000000',{
        placeholder: '08______________'
    }).on('focus', function() {
        if(!this.value) $(this).val('08');
        else $(this).val($(this).val());
    }).on('keyup', function() {
        var str = $(this).val();
        if (str.length < 2) $(this).val('08');
        else if (!str.startsWith('08')) $(this).val('08'+str);
    });
    $('#reg_id').mask('0000000000000000');
    $('#birth_date').mask('00/00/0000',{
        placeholder: "DD/MM/YYYY"
    });
    $('#full_name').mask('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', {
        'translation': {
            X: {pattern: /[A-Za-z ]/}
        },
        onKeyPress: function (value, event) {
            event.currentTarget.value = value.toUpperCase();
        }
    });
    $('#easypay_id').mask('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', {
        'translation': {
            X: {pattern: /[A-Za-z0-9]/}
        },
        onKeyPress: function (value, event) {
            event.currentTarget.value = value.toUpperCase();
        }
    });
    $('#mother_name').mask('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', {
        'translation': {
            X: {pattern: /[A-Za-z ]/}
        },
        onKeyPress: function (value, event) {
            event.currentTarget.value = value.toUpperCase();
        }
    });
});