<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page session="true"%>
<c:set var="now" value="<%=new java.util.Date()%>"/>
<c:set var="context" value="${pageContext.request.contextPath}" />

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />"/>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" href="<c:url value="/css/bootstrap-select.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/ajax-bootstrap-select.min.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/datepicker.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/ion.rangeSlider.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/ion.rangeSlider.skinHTML5.css" />" />
    <link rel="stylesheet" href="<c:url value="/css/my.css?${uuid}" />" />

    <title>FINEASY</title>
    <script>
        validationUrl = "<c:url value="/form/validation/" />";
        contextPath = "<c:url value="/" />";
        defaultLang = "${lang}";
        sessionId = "${pageContext.session.id}";
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WTRJL9Z');</script>
    <!-- End Google Tag Manager -->

</head>
<body style="background-color: #4e555b">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WTRJL9Z"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="container" style="padding: 20px; background-color: #4e555b;">
    <div class="container" style="border-radius:5px; padding: 20px; background-color: #ffffff;">

        <div class="container border shadow-sm p-3 mb-2 bg-white rounded" >
            ${website_header_text1}
        </div>



        <form id="form1" method="post">

            <div class="container border shadow-sm p-3 mb-2 bg-white rounded" >
                <div class="form-row">
                    <!-- full name field -->
                    <div class="form-group col-md-6">
                        <label for="full_name" class="my-label">${label_full_name}</label>
                        <input type="text" class="form-control" name="_db_full_name" id="full_name" placeholder="${placeholder_full_name}"
                               minlength="5"
                               value="${formData.full_name}"
                               required autocomplete="off"
                               data-parsley-wordcount="2"
                               data-parsley-validation-threshold="3"
                               data-parsley-debounce="300"/>
                    </div>
                    <!-- mobile -->
                    <div class="form-group col-md-6">
                        <label for="mobile" class="my-label">${label_mobile}</label>
                        <input autocomplete="off" type="text" class="form-control"
                               value="<c:out value="${empty formData.mobile ? '08' : formData.mobile}"/>"
                               data-parsley-length="[10, 15]"
                               data-parsley-required="true"
                               data-parsley-validation-threshold="2"
                               name="_db_mobile" id="mobile"/>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="easypay_id" class="my-label">${label_easypay_id}</label>
                        <input type="text" class="form-control" name="_db_easypay_id" id="easypay_id" placeholder="${placeholder_easypay_id}"
                               data-parsley-length="[5,10]"
                               autocomplete="off"
                               value="${formData.easypay_id}"
                               data-parsley-required="true"
                               data-parsley-debounce="300"/>
                    </div>

                    <div class="form-group col-md-6">
                        &nbsp;
                    </div>
                </div>


                <div id="container-slider" style="display: none;">
                    <div  class="form-row border shadow-sm p-3 mb-2 bg-white rounded">
                        <div class="form-group col-md-6" style="padding: 5px 20px 0px 20px;">
                            <div style="font-size:1em;margin-bottom: 10px;" class="font-weight-bold">${label_slider_amount}</div>
                            <div class="rangeslider-wrap">
                                <input type="text" class="form-control" id="amount_id" name="_db_loan_amount" value="" />
                            </div>
                        </div>
                        <div class="form-group col-md-6" style="padding: 5px 20px 0px 20px;">
                            <div style="font-size:1em;margin-bottom: 10px;" class="font-weight-bold">${label_slider_tenor}</div>
                            <div class="rangeslider-wrap">
                                <input type="text" class="form-control" id="tenor_id" name="_db_loan_term" value="" />
                            </div>
                        </div>

                        <div class="container">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div style="font-size:0.8em;" class="font-weight-bold">${label_slider_amount}</div>
                                    <div style="display: inline-block;font-size:0.8em;">Rp</div>
                                    <div id="amount_loan" class="font-weight-bold" style="font-size:1.5em;display: inline-block;"></div>
                                </div>
                            </div>
                            <div class="row">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div  class="form-row border shadow-sm p-3 mb-2 bg-white rounded" style="margin-top:0px;">
                <div class="form-group form-check">
                    <input type="checkbox" name="consent_privacy" class="form-check-input" id="consent_privacy"
                           data-parsley-required-message="${msg_unchecked_consent}"
                           data-parsley-required="true"
                           data-parsley-errors-container="#consent_privacy_error">
                    <label class="form-check-label" for="consent_privacy">${msg_consent_privacy}</label>
                    <div id="consent_privacy_error"></div>
                </div>
            </div>

            <style>
                img.tutorial {
                    width: 30%;
                }
                @media only screen and (max-width: 600px) {
                    img.tutorial {
                        width: 90%;
                    }
                }
            </style>

            <input type="hidden" name="xcode" value="${_csrf.token}"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
        <div class="container text-center">
            <button type="button" id="submit" class="btn btn-primary">Submit</button>
        </div>
        <div  class="form-row border shadow-sm p-3 mb-2 bg-white rounded" style="margin-top:10px;">
            <div class="form-group form-check text-center">
                <img src="<c:url value="/img/img1.png" />" class="tutorial"/>
                <img src="<c:url value="/img/img2.png" />" class="tutorial"/>
                <img src="<c:url value="/img/img3.png" />" class="tutorial"/>
            </div>
        </div>
    </div>
</div>

<div id="modal-block" class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width: 48px">
            <span class="fa fa-spinner fa-spin fa-3x"></span>
        </div>
    </div>
</div>

<script src="<c:url value="/js/lib/jquery-3.3.1.js" />"></script>
<script src="<c:url value="/custom/script/startup.js" />" type="text/javascript"></script>


</body>
</html>