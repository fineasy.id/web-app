$("#amount_id").ionRangeSlider({
    grid: true,
    prefix: "Rp ",
    prettify_enabled: true,
    prettify_separator: ".",
    values: [0, 10, 100, 1000, 10000, 100000, 1000000]
});


$("#tenor_id").ionRangeSlider({
    grid: true,
    postfix: ' hari',
    prettify_enabled: true,
    prettify_separator: ".",
    values: [20,30,60,120,180,210]
});