package id.fineasy.web.app.controller;

import id.fineasy.web.app.utils.DataUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

@RestController
public class DataProvider {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    DataUtils dataUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value="/custom/data/loan-matrix.json", produces = "application/json;charset=UTF-8")
    public String data(HttpServletRequest request){

        String productId = ""+request.getSession().getAttribute("defaultProductId");
        int loanSeq = (int) request.getSession().getAttribute("loanSeq");

        log.debug("Get loan matrix based on product "+productId+", seq: "+loanSeq);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("productId", productId);
        params.addValue("loanSeq", loanSeq);

        String sql = " SELECT DISTINCT amount FROM `product_matrix_mca` WHERE 1 AND product_id=:productId and sequence=:loanSeq AND is_active=1 AND is_on_web=1 ORDER BY amount ";
        JSONArray amountJson = new JSONArray(namedJdbcTemplate.queryForList (sql, params, Long.class));

        sql = " SELECT DISTINCT tenor FROM `product_matrix_mca` WHERE 1 AND product_id=:productId and sequence=:loanSeq AND is_active=1 AND is_on_web=1 ORDER BY tenor ";
        JSONArray tenorJson = new JSONArray(namedJdbcTemplate.queryForList (sql, params, Integer.class));

        sql = " SELECT DISTINCT amount,tenor,charge FROM `product_matrix_mca` WHERE 1 AND product_id=:productId and sequence=:loanSeq AND is_active=1 AND is_on_web=1 ORDER BY amount ";
        List<Map<String,Object>> tenors = namedJdbcTemplate.queryForList (sql, params);

        JSONObject jsonCharge = new JSONObject();
        for (Map<String,Object> tenor: tenors) {
            String amountKey = ""+(long) tenor.get("amount");
            String tenorKey = ""+tenor.get("tenor");
            if (!jsonCharge.has(amountKey)) jsonCharge.put(amountKey, new JSONObject());
            jsonCharge.getJSONObject(amountKey).put(tenorKey, tenor.get("charge"));
        }

        JSONObject jsonResult = new JSONObject();
        jsonResult.put("amount", amountJson);
        jsonResult.put("tenor", tenorJson);
        jsonResult.put("charge", jsonCharge);

        return jsonResult.toString(4);
    }
}
