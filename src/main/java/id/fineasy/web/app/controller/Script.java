package id.fineasy.web.app.controller;

import id.fineasy.web.app.utils.DataUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.UUID;

@Controller
public class Script {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    DataUtils dataUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());



    @RequestMapping(value={"/custom/script/{scriptName}.js","/custom/script/{uuid}/{scriptName}.js"}, produces = "application/javascript;charset=UTF-8")
    public String script(ModelMap model, HttpServletRequest request, @PathVariable("scriptName") String scriptName){

        JSONObject formData = (request.getSession().getAttribute("formData")==null)?new JSONObject():(JSONObject) request.getSession().getAttribute("formData");

        String lang = (request.getSession().getAttribute("lang")==null)?"id":""+request.getSession().getAttribute("lang");
        model.addAllAttributes(dataUtils.getAllWebText(lang));

        model.addAttribute("uuid", UUID.randomUUID().toString());
        model.addAttribute("lang", lang);
        model.addAttribute("formData", formData.toString());

        return "script/"+scriptName;
    }
}
