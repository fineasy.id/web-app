package id.fineasy.web.app.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.fineasy.web.app.component.MessageSender;
import id.fineasy.web.app.utils.ConfigUtils;
import id.fineasy.web.app.utils.DataUtils;
import id.fineasy.web.app.utils.HttpUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.*;

import static id.fineasy.web.app.config.ActiveMQConfig.QUEUE_NEW_APPLICATION_GATE;

@Controller
public class Index {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    DataUtils dataUtils;

    @Autowired
    ConfigUtils configUtils;

    @Autowired
    MessageSender messageSender;

    String refId = null;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value={"/","/reloan"}, method = RequestMethod.GET)
    public String indexGet(ModelMap model, HttpServletRequest request, HttpServletResponse response){
        if (request.getSession().getAttribute("refId")==null) {
            refId = UUID.randomUUID().toString();
            request.getSession().setAttribute("refId", refId);
        } else {
            refId = request.getSession().getAttribute("refId").toString();

            // check if refId is still valid.
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("refId", refId);
            List<Boolean> result = namedJdbcTemplate.queryForList("SELECT is_processed FROM application_tmp WHERE ref_id=:refId ", params, Boolean.class);
            if (result.size()>0 && result.get(0)) {
                // ref is already processed!
                HttpUtils.cleanSession(request);
                try {
                    response.sendRedirect(request.getServletPath()+"?refresh="+ DateTime.now().getMillis());
                } catch (Exception e) {
                }
                return "blank";
            }
        }

        String defaultProductId = configUtils.getConfig("default.product.id");
        log.debug("Default product ID: "+defaultProductId);
        if (request.getSession().getAttribute("defaultProductId")==null) {
            request.getSession().setAttribute("defaultProductId", defaultProductId);
            request.getSession().setAttribute("loanSeq", (int) 1);
        }

        String lang = (request.getSession().getAttribute("lang")==null)?"id":""+request.getSession().getAttribute("lang");
        if ((request.getParameter("lang")!=null)) {
            lang = request.getParameter("lang");
            request.getSession().setAttribute("lang", lang);
        }
        //if (lang!="id"&&lang!="en") lang="id";

        log.debug("Default lang: "+lang);

        JSONObject formData = (request.getSession().getAttribute("formData")!=null)?(JSONObject) request.getSession().getAttribute("formData"):new JSONObject();
        if (formData!=null) {
            Map<String, Object> formDataMap = new HashMap<>();
            try {
                ObjectMapper mapper = new ObjectMapper();
                formDataMap = mapper.readValue(formData.toString(), new TypeReference<Map<String, Object>>() {
                });
                model.addAttribute("formData", formDataMap);
            } catch (Exception e) {

            }
        }

        model.addAllAttributes(dataUtils.getAllWebText(lang));
        model.addAttribute("martialStatuses", dataUtils.getMartialStatuses(lang));
        model.addAttribute("childrenList", dataUtils.getChildrenList(lang));
        model.addAttribute("educationList", dataUtils.getEducationList(lang));
        model.addAttribute("businessYearsList", dataUtils.getBusinessYears(lang));
        model.addAttribute("loanPurposeList", dataUtils.getLoanPurposeList(lang));

        model.addAttribute("uuid", refId);
        model.addAttribute("lang", lang);

        boolean isReloan = (request.getServletPath().equals("/reloan"))?true:false;
        model.addAttribute("isReloan", isReloan);

        log.debug("is reloan:? "+isReloan);
        if (isReloan) {
            request.getSession().setAttribute("loanSeq",2);
        } else {
            request.getSession().setAttribute("loanSeq",1);
        }

        HttpSession session = request.getSession();
        Cookie cookie = new Cookie("JSESSIONID", session.getId());
        cookie.setMaxAge(3600);
        response.addCookie(cookie);

        return "index";
    }

    @RequestMapping(value={"/push/{tmpId}"}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> push(ModelMap model, HttpServletRequest request, HttpServletResponse response, @PathVariable("tmpId") String tmpId){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);

        messageSender.send(QUEUE_NEW_APPLICATION_GATE, "tmpId", tmpId);
        return new ResponseEntity<String>("OK", headers, HttpStatus.OK);
    }


    @RequestMapping(value={"/","/reloan"}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> indexPost(ModelMap model, HttpServletRequest request, HttpServletResponse response){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpStatus httpStatus = HttpStatus.OK;

        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", false);

        List<String> paramNames = new ArrayList<String>();
        Enumeration<String> enumParamNames = request.getParameterNames();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sqlDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        MapSqlParameterSource params = new MapSqlParameterSource();

        List<String> fieldNames = new ArrayList<String>();
        List<String> values = new ArrayList<>();
        List<String> updates = new ArrayList<>();

        try {
            while (enumParamNames.hasMoreElements()) {
                String name = enumParamNames.nextElement();
                if (name.startsWith("_db_")) {
                    String fieldName = name.replaceFirst("_db_","");
                    String fieldValue = request.getParameter(name);
                    if (fieldName.equals("birth_date")) {
                        fieldValue = sqlDf.format(df.parse(fieldValue));
                    } else if (fieldName.equals("mobile")) {
                        fieldValue = fieldValue.replaceFirst("0","+62");
                    }
                    params.addValue(fieldName, fieldValue);
                    fieldNames.add(fieldName);
                    values.add(":"+fieldName);
                    updates.add(fieldName+"=:"+fieldName);
                }
            }

            refId = ""+request.getSession().getAttribute("refId");

            fieldNames.add("ref_id");
            values.add(":ref_id");
            updates.add("ref_id"+"=:ref_id");
            params.addValue("ref_id", request.getSession().getAttribute("refId"));

            fieldNames.add("is_finished");
            values.add(":is_finished");
            updates.add("is_finished"+"=:is_finished");
            params.addValue("is_finished", 1);

            fieldNames.add("process_end");
            values.add(":process_end");
            updates.add("process_end"+"=:process_end");
            params.addValue("process_end", sqlDf.format(new Date()));

            String sql = " INSERT INTO application_tmp ("+ StringUtils.join(fieldNames,",") +") VALUES ("+StringUtils.join(values,",")+") ";
            sql+= " ON DUPLICATE KEY UPDATE "+StringUtils.join(updates,",");
            log.debug("Update SQL: "+sql);
            namedJdbcTemplate.update(sql, params);

            jsonResult.put("success", true);

            HttpUtils.cleanSession(request);

            messageSender.send(QUEUE_NEW_APPLICATION_GATE, "tmpId", getApplicationByRef(refId));
        } catch (Exception e) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            log.error("Error submitting: "+e,e);
        }

        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }



    @RequestMapping(value={"/thank", "/sorry", "/failed"}, method = RequestMethod.GET)
    public String thank(ModelMap model, HttpServletRequest request, HttpServletResponse response){
        String lang = getDefaultLang(request);

        Map<String,Object> webTexts = dataUtils.getAllWebText(lang);

        model.addAttribute("uuid", UUID.randomUUID().toString());
        model.addAllAttributes(webTexts);

        HttpUtils.cleanSession(request);

        String message = "";
        if (request.getServletPath().equals("/thank")) {
            message = ""+webTexts.get("thank_msg");
        }
        else if (request.getServletPath().equals("/sorry")) {
            message = ""+webTexts.get("sorry_msg");
        }
        else if (request.getServletPath().equals("/failed")) {
            message = ""+webTexts.get("failed_msg");
        }

        model.addAttribute("message", message);

        return "thank";
    }

    private String getDefaultLang(HttpServletRequest request) {
        String lang = (request.getSession().getAttribute("lang")==null)?"id":""+request.getSession().getAttribute("lang");
        if ((request.getParameter("lang")!=null)) {
            lang = request.getParameter("lang");
            request.getSession().setAttribute("lang", lang);
        }
        return lang;
    }

    private Long getApplicationByRef(String refId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("refId", refId);
        String sql = " SELECT tmp_id FROM application_tmp WHERE ref_id=:refId ";
        Long tmpId = namedJdbcTemplate.queryForObject(sql, params, Long.class);
        return tmpId;
    }
}
