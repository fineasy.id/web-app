package id.fineasy.web.app.controller;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Controller
public class FormValidation {

    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private String lang = "id";
    private HttpServletRequest request;
    private SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat sqlDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @ResponseBody
    @RequestMapping(value="/form/update/{fieldId}", method = RequestMethod.POST)
    public String validate(HttpServletRequest request, @PathVariable("fieldId") String fieldId){
        JSONObject result = new JSONObject();
        result.put("success", true);

        try {
            if (request.getParameter("n")!=null && !request.getParameter("n").startsWith("_db_")) return result.toString();
            String fieldValue = request.getParameter("v").trim();
            String fieldName = request.getParameter("n").trim().replaceFirst("_db_","");

            log.info("Validation name: "+fieldId+", field value: "+fieldValue);
            this.request = request;

            lang = (request.getSession().getAttribute("lang")==null)?"id":""+request.getSession().getAttribute("lang");

            JSONObject updateData = new JSONObject();

            updateData.put(fieldName, fieldValue);
            updateData.put("session_id", request.getSession().getId());
            updateData.put("ref_id", request.getSession().getAttribute("refId"));
            updateData.put("product_id", request.getSession().getAttribute("defaultProductId"));
            updateData.put("loan_seq", request.getSession().getAttribute("loanSeq"));

            // parse data
            updateData = parseName(updateData, fieldName, fieldValue);
            updateData = parseBirthDate(updateData, fieldName, fieldValue);

            log.debug("Form Data: "+updateData.toString(4));

            updateFormData(updateData);
        } catch (Exception e) {
            log.error("Error update form: "+e,e);
            result.put("success", false);
        }


        //request.getSession().setAttribute("formData", formData);
        return result.toString();
    }


    private void parseData(String fieldName, String fieldValue) {
        //parseName(fieldName,fieldValue);
        //parseBirthDate(fieldName,fieldValue);
        //parseMotherName(fieldName,fieldValue);
    }

    private void parseMobile(String fieldName, String fieldValue) {
        if (!fieldName.equals("mobile")) return;
    }

    private JSONObject parseName(JSONObject formData, String fieldName, String fieldValue) {
        if (fieldName.equals("full_name")||fieldName.equals("mother_name")) {
            fieldValue = fieldValue.trim().replaceAll("[ ]{2,100}"," ").toUpperCase();
            log.debug("Fixed name: "+ fieldValue);
            String[] names = fieldValue.split(" ");
            String firstName = "";
            String lastName = "";
            String midName = null;
            for (int i=0;i<names.length;i++) {
                if (i==0) firstName = names[0];
                else if (i==1 && names.length>2) midName = names[i];
                else lastName+=names[i]+" ";
            }
            lastName = lastName.trim();
            log.debug("First: "+firstName+", mid name: "+midName+", last name: "+lastName);
            if (fieldName.equals("full_name")) {
                formData.put("first_name", firstName);
                formData.put("mid_name", midName);
                formData.put("last_name", lastName);
            }
            formData.put("full_name", fieldValue);
        }
        return formData;
    }

    private JSONObject parseBirthDate(JSONObject updateData, String fieldName, String fieldValue) {
        if (fieldName.equals("birth_date")) {
            try {
                log.debug("Date is "+fieldValue);
                Date dt = df.parse(fieldValue);
                log.debug("BOD: "+sqlDf.format(dt));
                updateData.put(fieldName, sqlDf.format(dt));
                log.debug("Parsed to be: "+dt);
            } catch (ParseException e) {
                log.error("Error: "+e);
            }
        }
        return updateData;
    }


    private void updateFormData(JSONObject updateData) throws ParseException {

        JSONObject formData = (request.getSession().getAttribute("formData")==null)?new JSONObject():(JSONObject) request.getSession().getAttribute("formData");

        updateData.put("process_end", sqlDf.format(new Date()));
        List<String> fieldNames = new ArrayList<String>();
        fieldNames.addAll(updateData.keySet());
        log.debug(""+fieldNames);
        MapSqlParameterSource params = new MapSqlParameterSource();

        List<String> values = new ArrayList<>();
        List<String> updates = new ArrayList<>();
        for (String fieldName: fieldNames) {
            formData.put(fieldName, updateData.get(fieldName));
            if (fieldName.equals("birth_date")) {
                formData.put(fieldName, df.format(sqlDf.parse(""+updateData.get(fieldName))));
            }
            else if (fieldName.equals("mobile")) {
                updateData.put(fieldName, updateData.get(fieldName).toString().replaceFirst("0","+62"));
            }

            params.addValue(fieldName, updateData.get(fieldName));
            log.debug("SQL params: "+fieldName+", "+updateData.get(fieldName));
            values.add(":"+fieldName);
            updates.add(fieldName+"=:"+fieldName);
        }
        String sql = " INSERT INTO application_tmp ("+ StringUtils.join(fieldNames,",") +") VALUES ("+StringUtils.join(values,",")+") ";
        sql+= " ON DUPLICATE KEY UPDATE "+StringUtils.join(updates,",");
        log.debug("Update SQL: "+sql);
        namedJdbcTemplate.update(sql, params);
        request.getSession().setAttribute("formData", formData);
    }
}
