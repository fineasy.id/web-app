package id.fineasy.web.app.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

public class HttpUtils {
    public static void cleanSession(HttpServletRequest request) {
        Enumeration<String> attributes = request.getSession().getAttributeNames();
        while (attributes.hasMoreElements()) {
            String attName = attributes.nextElement();
            request.getSession().removeAttribute(attName);
        }
    }
}
