package id.fineasy.web.app.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataUtils {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;

    public Map<String,Object> getAllWebText(String lang) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("lang", lang);

        String sql = " SELECT text_key,content FROM web_text WHERE lang=:lang AND is_list=0 ORDER BY display_order ";
        List<Map<String,Object>> rows = namedJdbcTemplate.queryForList(sql, params);
        Map<String,Object> texts = new HashMap<>();

        for(Map<String,Object> row: rows) {
            texts.put(""+row.get("text_key"), row.get("content"));
        }

        return texts;
    }

    public List<Map<String,Object>> getMartialStatuses(String lang) {
        String sql = " SELECT s.id,IFNULL(content,s.name) as `name` FROM contact_marital_status s LEFT JOIN web_text t ON (text_key=CONCAT('marital_status_',s.id) AND lang=:lang)  ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("lang", lang);
        return namedJdbcTemplate.queryForList(sql, params);
    }

    public List<Map<String,Object>> getChildrenList(String lang) {
        String sql = " SELECT s.id,IFNULL(content,s.name) as `name` FROM contact_children s LEFT JOIN web_text t ON (text_key=CONCAT('children_',s.id) AND lang=:lang) ORDER BY s.display_order  ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("lang", lang);
        return namedJdbcTemplate.queryForList(sql, params);
    }

    public List<Map<String,Object>> getBusinessYears(String lang) {
        String sql = " SELECT s.id,IFNULL(content,s.name) as `name` FROM contact_business_years s LEFT JOIN web_text t ON (text_key=CONCAT('business_years_',s.id) AND lang=:lang) ORDER BY s.id  ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("lang", lang);
        return namedJdbcTemplate.queryForList(sql, params);
    }

    public List<Map<String,Object>> getEducationList(String lang) {
        String sql = " SELECT s.id,IFNULL(content,s.name) as `name` FROM contact_edu s LEFT JOIN web_text t ON (text_key=CONCAT('edu_',s.id) AND lang=:lang) ORDER BY s.id  ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("lang", lang);
        return namedJdbcTemplate.queryForList(sql, params);
    }

    public List<Map<String,Object>> getLoanPurposeList(String lang) {
        String sql = " SELECT s.id,IFNULL(content,s.name) as `name` FROM application_loan_purpose s LEFT JOIN web_text t ON (text_key=CONCAT('loan_purpose_',s.id) AND lang=:lang) ORDER BY s.id  ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("lang", lang);
        return namedJdbcTemplate.queryForList(sql, params);
    }
}
